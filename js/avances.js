    $(document)
    .on('click', 'a[href*="#"]', function() {
        if ( this.hash && this.pathname === location.pathname ) {
        $.bbq.pushState( '#/' + this.hash.slice(1) );
        return false;
        }
    })

    .ready(function() {
        //ESTA PRIMERA PARTE ES DE AVANCE LENTO
        $(window).bind('hashchange', function(event) {
            var tgt = location.hash.replace(/^#\/?/,'');
            if ( document.getElementById(tgt) ) {
            $.smoothScroll({scrollTarget: '#' + tgt});
            }
        });

        $(window).trigger('hashchange');

        //ESTA SEGUNDA PARTE ES DE FLECHITA HACIA ARRIBA
        jQuery("#IrArriba").hide();
        jQuery(function () {
            jQuery(window).scroll(function () {
                if (jQuery(this).scrollTop() > 200) {       //200 DISTANCIA DESDE EL TOP QUE APARECE FLECHITA
                    jQuery('#IrArriba').fadeIn();
                } else {
                    jQuery('#IrArriba').fadeOut();
                }
            });
            jQuery('#IrArriba a').click(function () {
                jQuery('body,html').animate({scrollTop: 0}, 800);   //800 TIEMPO DE RETARDO EN SUBIR
                return false;
            });
        });

    });
